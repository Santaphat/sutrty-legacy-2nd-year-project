#pragma once
#include "SpriteSheet.h"
#include "Map.h"
#include <string>
class Editor
{
private:
	SpriteSheet * sheet = nullptr;
	Map * map = nullptr;
	int currentSpriterow;
	int currentSpritecol;
	float * TextureCoord;
	int currentType;
public:
	void createMap(int row, int col, std::string backgroundfilename);
	void createSpriteSheet(int row, int col,std::string filename);
	void setcurrentsprite(int sr, int sc);
	void setcurrentwidthheight(int width, int height);
	void setType(int type);
	int getType();
	int getspritecurrentrow();

	void loadfile(std::string filename);
	float * getTextCoord();
	Map * getMapcurrentstatus();
	SpriteSheet * getSpritesheetcurrentstatus();
	Editor();
	~Editor();

	SpriteSheet * getSpriteSheet();
	Map * getMap();
};


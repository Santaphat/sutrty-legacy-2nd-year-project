#pragma once
#include "MeshVbo.h"
#include "glm\glm.hpp"
class LineMesh : public MeshVbo{

public:
	LineMesh();
	virtual void loadData();
	virtual string getMeshName();
	virtual void render();
};


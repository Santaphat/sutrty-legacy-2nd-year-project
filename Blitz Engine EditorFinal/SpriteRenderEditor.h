#pragma once
#include "GLRenderer.h"

class SpriteRenderEditor : public GLRenderer
{
private:
	static SpriteRenderEditor * instance;
protected:
	GLuint colorId;
	GLuint modeId;
	GLuint textureId = -1;
	GLuint alphaId;

	int textureLocation;
	virtual bool initialize(string vertexShaderFile, string fragmentShaderFile);
	virtual void setMeshAttribId(MeshVbo * shape);

public:
	SpriteRenderEditor();
	~SpriteRenderEditor();
	virtual void render();
	void BeginRender();
	void EndRender();
	GLuint initTexture(string filename);
	void setProjectionMatrix(int row , int col);
	static SpriteRenderEditor * getinstance();
	void setModelMatrix(glm::mat4 mm);
	void setmode(int mod);
	void setColor(glm::vec4 color);
	MeshVbo * getMesh(std::string meshname);
	glm::mat4 getProjectonMatrix();
};

#pragma once
#include "GlRenderEditor.h"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"


#include "stb_image.h"


GlRenderEditor * GlRenderEditor::instance = nullptr;

GlRenderEditor * GlRenderEditor::getinstance() {
	if (instance == nullptr) {
		instance = new GlRenderEditor();
	}
	return instance;

}
void GlRenderEditor::setModelMatrix(glm::mat4 mm)
{
	glUniformMatrix4fv(matrixId, 1, GL_FALSE, glm::value_ptr(mm));
}
void GlRenderEditor::setmode(int mod)
{
	glUniform1i(modeId, mod);
}

void GlRenderEditor::setColor(glm::vec4 color)
{
	glUniform3fv(colorId, 1, glm::value_ptr(glm::vec3(color.r, color.g, color.b)));
	glUniform1f(alphaId, color.a);
}

void GlRenderEditor::setTexture(GLuint textureId)
{
	glBindTexture(GL_TEXTURE_2D, textureId);
}

MeshVbo * GlRenderEditor::getMesh(std::string meshname)
{
	if (shapes.find(meshname) == shapes.end()) {
		return nullptr;
	}
	else {
		return shapes[meshname];
	}
}

glm::mat4 GlRenderEditor::getProjectonMatrix()
{
	return projectionMatrix;
}

void GlRenderEditor::setOrthoPos(float orthoX, float orthoY)
{
	this->orthoX = orthoX;
	this->orthoY = orthoY;
}

void GlRenderEditor::setDefOrthoZoom(int size)
{
	DeforthoZoomX = size;
	DeforthoZoomY = size;
}

float GlRenderEditor::getOrthoPosx()
{
	return orthoX;
}

float GlRenderEditor::getOrthoPosy()
{
	return orthoY;
}

float GlRenderEditor::getDefOrthoZoomX()
{
	return DeforthoZoomX;
}

float GlRenderEditor::getDefOrthoZoomY()
{
	return DeforthoZoomX;
}

GlRenderEditor::GlRenderEditor() :GLRenderer(800, 600)
{
	orthoX = 0.0f;
	orthoY = 0.0f;
	DeforthoZoomX = 10.0f;
	DeforthoZoomY = 10.0f;

}

GlRenderEditor::~GlRenderEditor()
{
}

void GlRenderEditor::render()
{
}

bool GlRenderEditor::initialize(string vertexShaderFile, string fragmentShaderFile)
{
	bool loadShader = GLRenderer::initialize(vertexShaderFile, fragmentShaderFile);
	if (!loadShader) {
		return false;
	}

	//Setting color attribute id
	//Get vertex attribute location
	textureLocation = glGetAttribLocation(gProgramId, "inTexCoord");
	if (textureLocation == -1)
	{
		cout << "inTexCoord is not a valid glsl program variable" << endl;
		return false;
	}

	glEnableVertexAttribArray(textureLocation);
	return true;
}

void GlRenderEditor::initTexture(string filename)
{
	glGenTextures(1, &textureId);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true);
	unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrChannels, 0);
	//unsigned char *data = stbi_load("C:/Users/KuragariRyuu/Desktop/Test.png", &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);
}

void GlRenderEditor::setDefaultProjectionMatrix() {

	//projectionMatrix = glm::ortho(0.f, (float)col, 0.f, (float)row);
	projectionMatrix = glm::ortho(orthoX, orthoX + DeforthoZoomX, orthoY, orthoY + DeforthoZoomY);
}

void GlRenderEditor::setMeshAttribId(MeshVbo * shape)
{
	shape->setAttribId(gVertexPos2DLocation, textureLocation);
}

void GlRenderEditor::BeginRender()
{
	// Clear color buffer
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Inizlizing PNG Transparentcy
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	// Update window with OpenGL rendering
	glUseProgram(gProgramId);
	//Set up matrix uniform
	matrixId = glGetUniformLocation(gProgramId, "mMatrix");
	colorId = glGetUniformLocation(gProgramId, "colorVec");
	modeId = glGetUniformLocation(gProgramId, "mode");
	alphaId = glGetUniformLocation(gProgramId, "alpha");
	glBindTexture(GL_TEXTURE_2D, textureId);

}

void GlRenderEditor::EndRender() {
	//Unbind program
	glUseProgram(NULL);
}


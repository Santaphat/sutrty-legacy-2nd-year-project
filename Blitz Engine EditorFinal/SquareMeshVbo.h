#pragma once
#include "MeshVbo.h"

class SquareMeshVbo : public MeshVbo {
public:
  virtual void loadData();
  SquareMeshVbo();
  virtual string getMeshName();
  virtual void render();
};
#pragma once
#include <wx/wx.h>
#include "Editor.h"
#include "MyGlCanvas.h"
#include "SpriteGLCanvas.h"

class EditorFrame : public wxFrame
{
private:
	Editor * editor;
	int saved = 1;
public:
	EditorFrame(const wxString& title);
	void OpenSprite(wxCommandEvent& event);
	void NewMap(wxCommandEvent& event);
	void OnKeyDown(wxKeyEvent& event);
	void RadioButFun(wxCommandEvent& event);
	void SaveMenu(wxCommandEvent& event);
	void LoadMenu(wxCommandEvent& event);
	MyGLCanvas *glcanvas;
	SpriteGLCanvas *SpriteGL;

	DECLARE_EVENT_TABLE()

	enum
	{
		wxID_Load = wxID_HIGHEST + 11,
		wxID_Save = wxID_HIGHEST + 9,
		wxID_CollisionRadio = wxID_HIGHEST + 5,
		wxID_PlatformRadio = wxID_HIGHEST + 6,
		wxID_ItemRadio = wxID_HIGHEST + 4,
		wxID_MonsterRadio = wxID_HIGHEST + 7,
		wxID_PlayerRadio = wxID_HIGHEST + 8,
		wxID_NewMap = wxID_HIGHEST + 3,
		wxID_NewProject = wxID_HIGHEST + 2,
		wxID_PANEL_SPRITE = wxID_HIGHEST +1,
		wxID_PANEL_GL = wxID_PANEL_SPRITE + 10
	};
};
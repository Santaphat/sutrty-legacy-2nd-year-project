#pragma once
#include "SpriteMesh.h"


void SpriteMesh::loadData()
{
	//VBO data
	GLfloat vertexData[] =
	{
		-0.5f, -0.5f,
		0.5f, -0.5f,
		0.5f,  0.5f,
		-0.5f,  0.5f
	};

	GLfloat textureData[] =
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f,  1.0f,
		0.0f,  1.0f
	};

	//Create VBO
	glGenBuffers(1, &(this->posVboId));
	glBindBuffer(GL_ARRAY_BUFFER, this->posVboId);
	glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(GLfloat), vertexData, GL_STATIC_DRAW);

	//Create VBO
	glGenBuffers(1, &(this->textureId));
	glBindBuffer(GL_ARRAY_BUFFER, this->textureId);
	glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(GLfloat), textureData, GL_STATIC_DRAW);


}

string SpriteMesh::getMeshName()
{
	return "Sprite";
}

void SpriteMesh::render()
{
	if (this->posAttribId != -1) {
		glBindBuffer(GL_ARRAY_BUFFER, this->posVboId);
		glVertexAttribPointer(this->posAttribId, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), NULL);

		glBindBuffer(GL_ARRAY_BUFFER, this->textureId);
		glVertexAttribPointer(this->textureAttribId, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), NULL);
	}
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}
void SpriteMesh::gridRender()
{
	if (this->posAttribId != -1) {
		glBindBuffer(GL_ARRAY_BUFFER, this->posVboId);
		glVertexAttribPointer(this->posAttribId, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), NULL);
	}
	glDrawArrays(GL_LINE_LOOP, 0, 4);
}

void SpriteMesh::setAttribId(GLuint posId, GLuint textureId)
{
	this->posAttribId = posId;
	this->textureAttribId = textureId;
}

void SpriteMesh::setdefultTextureCoord()
{
	GLfloat textureData[] =
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f,  1.0f,
		0.0f,  1.0f
	};

	glBindBuffer(GL_ARRAY_BUFFER, this->textureId);
	glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(GLfloat), textureData, GL_STATIC_DRAW);
}

void SpriteMesh::setTextureCoord(float * Texcoord)
{
	GLfloat* textureData;
	textureData = Texcoord;
	GLfloat temp;
	/*for (int i = 0; i < 8; i++) {
		textureData[i] = Texcoord[i];
	}*/
	glBindBuffer(GL_ARRAY_BUFFER, this->textureId);
	glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(GLfloat), textureData, GL_STATIC_DRAW);
}

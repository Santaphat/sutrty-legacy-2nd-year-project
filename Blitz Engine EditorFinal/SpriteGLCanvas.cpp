#pragma once
#include "SpriteGLCanvas.h"

SpriteGLCanvas::SpriteGLCanvas(wxPanel* parent,Editor * editor ) :
	wxGLCanvas(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, wxT("SpriteCanvas"))
{
	this->editor = editor;
	width = 300, height = 300, mapSize = 11;
	
	int argc = 1;
	char* argv[1] = { wxString((wxTheApp->argv)[0]).char_str() };

	m_context = new wxGLContext(this);
	SetCurrent(*m_context);

	SpriteRenderer = SpriteRenderEditor::getinstance();
	SpriteRenderer->initGL("shaders/basic/vertext.shd", "shaders/basic/fragment.shd");

	//Assigning All MeshVBO
	LineMesh * linevbo = new LineMesh;
	linevbo->loadData();
	SpriteRenderer->addShape(linevbo->getMeshName(), linevbo);

	SpriteMesh * spriteVbo = new SpriteMesh();
	spriteVbo->loadData();
	SpriteRenderer->addShape(spriteVbo->getMeshName(), spriteVbo);

	SquareMeshVbo * squarevbo = new SquareMeshVbo();
	squarevbo->loadData();
	SpriteRenderer->addShape(squarevbo->getMeshName(), squarevbo);
}

void SpriteGLCanvas::setMousePos(int x, int y)
{
	this->mousex = x;
	this->mousey = y;
}

int SpriteGLCanvas::getMousePosx()
{
	return mousex;
}

int SpriteGLCanvas::getMOusePosy()
{
	return  mousey;
}

void SpriteGLCanvas::render(wxPaintEvent& evt)
{
	SetCurrent(*m_context);
	wxPaintDC(this);

	SpriteRenderer->BeginRender();

	if (editor != nullptr && editor->getSpriteSheet() != nullptr) {
		editor->getSpriteSheet()->draw();
		editor->getSpriteSheet()->drawgrid();
		editor->getSpriteSheet()->drawSelect();
	}

	SpriteRenderer->EndRender();

	SwapBuffers();
}

void SpriteGLCanvas::resizeCanvas(wxSizeEvent & event)
{
	wxSize size = this->GetSize();
	SpriteRenderer->setViewport(size.x, size.y);
	this->Refresh();
}

void SpriteGLCanvas::OnKeyDownEvent(wxKeyEvent & event)
{
}

void SpriteGLCanvas::MousePos(wxMouseEvent & event)
{
	if (editor->getSpriteSheet() != nullptr) {
		const wxPoint pt = wxGetMousePosition();
		int x = pt.x - this->GetScreenPosition().x;
		int y = pt.y - this->GetScreenPosition().y;
		//Calculate Row & Col

		int totalrow = editor->getSpriteSheet()->getRow();
		int totalcol = editor->getSpriteSheet()->getCol();
		int CanvasWidth = this->GetSize().x;
		int CanvasHeight = this->GetSize().y;
		
		editor->setcurrentwidthheight(CanvasWidth, CanvasHeight);

		float gridWidth = (float)CanvasWidth / (float)totalcol;
		float gridHeight = (float)CanvasHeight / (float)totalrow;

		int row = totalrow - (int)(y / gridHeight) - 1;
		int col = (int)(x / gridWidth);

		//wxLogMessage("Col:%d Row:%d", col, row);
		editor->setcurrentsprite(row, col);
		this->Refresh();
	}
	
}

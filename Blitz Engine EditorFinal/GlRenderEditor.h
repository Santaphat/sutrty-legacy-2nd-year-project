#pragma once
#include "GLRenderer.h"

class GlRenderEditor : public GLRenderer
{
private:
	static GlRenderEditor * instance;
	float orthoX;
	float orthoY;
	float DeforthoZoomX;
	float DeforthoZoomY;
	

protected:
	GLuint colorId;
	GLuint modeId;
	GLuint textureId = -1;
	GLuint alphaId;

	int textureLocation;
	virtual bool initialize(string vertexShaderFile, string fragmentShaderFile);
	virtual void setMeshAttribId(MeshVbo * shape);

public:
	GlRenderEditor();
	~GlRenderEditor();

	virtual void render();
	void BeginRender();
	void EndRender();

	void initTexture(string filename);
	static GlRenderEditor * getinstance();

	void setDefaultProjectionMatrix();
	void setModelMatrix(glm::mat4 mm);
	void setmode(int mod);
	void setColor(glm::vec4 color);
	void setTexture(GLuint textureId);

	MeshVbo * getMesh(std::string meshname);
	glm::mat4 getProjectonMatrix();

	void setOrthoPos(float orthoX, float orthoY);
	void setDefOrthoZoom(int size);
	float getOrthoPosx();
	float getOrthoPosy();

	float getDefOrthoZoomX();
	float getDefOrthoZoomY();
};

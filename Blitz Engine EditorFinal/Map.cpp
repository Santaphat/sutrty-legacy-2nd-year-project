#pragma once
#include "Map.h"
#include <fstream>
#include "glm/gtc/matrix_transform.hpp"


Map::Map(int row, int col, std::string backgroundfilename )
{
	this->row = row;
	this->col = col;
	backtexture = backgroundfilename;

	GlRenderEditor::getinstance()->setOrthoPos(0, 0);
	if (row < 10 && row < col) {
		GlRenderEditor::getinstance()->setDefOrthoZoom(row);
	}
	else if (col < 10 && col < row) {
		GlRenderEditor::getinstance()->setDefOrthoZoom(col);
	}
	else if(col == row && col < 10){
		GlRenderEditor::getinstance()->setDefOrthoZoom(col);
	}
	else {
		GlRenderEditor::getinstance()->setDefOrthoZoom(10);
	}

	GlGrid = new Grid*[row];
	for (int i = 0; i < row; i++) {
		GlGrid[i] = new Grid[col];
	}
}


Map::~Map()
{
}

void Map::drawbackground()
{
	SpriteMesh * BaRe = dynamic_cast<SpriteMesh*>(GlRenderEditor::getinstance()->getMesh("Sprite"));
	GlRenderEditor::getinstance()->setmode(1);
	GlRenderEditor::getinstance()->initTexture(backtexture);
	//GlRenderEditor::getinstance()->setTexture(backgroundID);
	GlRenderEditor::getinstance()->setDefaultProjectionMatrix();
	glm::mat4 modelMatrix = glm::mat4(1.f);

	modelMatrix = glm::scale(modelMatrix, glm::vec3(col, row, 1));
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.5f, 0.5f, 0.0f));
	modelMatrix = GlRenderEditor::getinstance()->getProjectonMatrix()*modelMatrix;
	GlRenderEditor::getinstance()->setModelMatrix(modelMatrix);
	BaRe->setdefultTextureCoord();
	BaRe->render();
}

void Map::drawgridline()
{
	LineMesh * liMe = dynamic_cast<LineMesh*>(GlRenderEditor::getinstance()->getMesh("Line"));
	GlRenderEditor::getinstance()->setmode(0);
	GlRenderEditor::getinstance()->setColor(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	glm::mat4 modelMatrix = glm::mat4(1.f);

	

	for (int coltemp = 0; coltemp <= col; coltemp++) {
		modelMatrix = glm::mat4(1.f);

		modelMatrix = glm::translate(modelMatrix, glm::vec3(coltemp, row / 2.0f, 0.0f));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(1, row, 1));
		modelMatrix = glm::rotate(modelMatrix, 90.0f * 3.14f / 180.0f, glm::vec3(0.0f, 0.0f, 1.0f));
		modelMatrix = GlRenderEditor::getinstance()->getProjectonMatrix()*modelMatrix;
		GlRenderEditor::getinstance()->setModelMatrix(modelMatrix);
		liMe->render();
	}
	for (int rowtemp = 0; rowtemp <= row; rowtemp++) {
		modelMatrix = glm::mat4(1.f);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(col / 2.0f, rowtemp, 0.0f));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(col, 1, 1));
		modelMatrix = GlRenderEditor::getinstance()->getProjectonMatrix()*modelMatrix;
		GlRenderEditor::getinstance()->setModelMatrix(modelMatrix);
		liMe->render();
	}

}

void Map::drawgrid() {
	GlRenderEditor::getinstance()->initTexture(spritetexture);
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < col; j++) {
				GlGrid[i][j].draw();
		}
	}
}

int Map::getrow()
{
	return row;
}

int Map::getcol()
{
	return col;
}

void Map::setGrid(int row, int col, int type, float * textCoord, string Textid)
{
	this->GlGrid[row][col] = Grid(type, textCoord, row, col, SpriteID);
}

void Map::setSpritefilename(std::string filename)
{
	spritetexture = filename;
}

void Map::savefile(std::string filename)
{
	if (spritetexture != "") {
		std::ofstream newfile(filename, std::ios_base::app);
		if (newfile.is_open()) {
			//Telling what background and sprite sheet
			newfile << spritetexture << std::endl;
			newfile << spritesheetrow << " " << spritesheetrow << std:: endl;
			newfile << backtexture << std::endl;
			newfile << row << " " << col << std::endl;
			//Mode
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					newfile << this->GlGrid[i][j].getType() << " ";
				}
				newfile << std::endl;
			}
			newfile << endl;
			for (int i = 0; i < col; i++) {
				for (int j = 0; j < row; j++) {
					for(int k = 0; k < 8; k++)
					newfile << this->GlGrid[i][j].gettextcoord(k) << " " ;
					newfile << std::endl;
				}
			}
		}
	}
}

void Map::setSpritesheetrowcol(int row, int col)
{
	spritesheetcol = col;
	spritesheetrow = row;
}





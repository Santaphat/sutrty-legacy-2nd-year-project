#include "CustomDialog.h"

CustomDialog::CustomDialog(wxWindow* parent)
	: wxDialog(parent, wxID_ANY, wxT("Input size of grid"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE)
{
	wxPanel* mainPanel = new wxPanel(this, -1);
	wxBoxSizer* mainLayout = new wxBoxSizer(wxVERTICAL);

	wxStaticText *sizeX = new wxStaticText(mainPanel, wxID_ANY, wxT("X:"));
	wxStaticText *sizeY = new wxStaticText(mainPanel, wxID_ANY, wxT("Y:"));
	TextX = new wxTextCtrl(mainPanel, wxID_ANY);
	TextY = new wxTextCtrl(mainPanel, wxID_ANY);

	wxButton * b = new wxButton(mainPanel, wxID_OK, _("OK"));

	wxBoxSizer* xBox = new wxBoxSizer(wxHORIZONTAL);
	xBox->Add(sizeX, 1);
	xBox->Add(TextX, 3);
	mainLayout->Add(xBox, 0, wxEXPAND | wxRIGHT | wxLEFT | wxUP, 10);

	wxBoxSizer* yBox = new wxBoxSizer(wxHORIZONTAL);
	yBox->Add(sizeY, 1);
	yBox->Add(TextY, 3);
	mainLayout->Add(yBox, 0, wxEXPAND | wxRIGHT | wxLEFT | wxUP, 10);

	mainLayout->Add(b, 0,wxALIGN_RIGHT| wxRIGHT| wxBOTTOM | wxUP,5);

	mainPanel->SetSizer(mainLayout);
}

wxString CustomDialog::GetTextX() {
	return TextX->GetValue();
}
wxString CustomDialog::GetTextY() {
	return TextY->GetValue();
}
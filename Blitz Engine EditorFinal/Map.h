#pragma once
#include<string>
#include "SpriteMesh.h"
#include "LineMesh.h"
#include "Grid.h"
#include "GlRenderEditor.h"
class Map{
private:
	int row;
	int col;
	int spritesheetrow;
	int spritesheetcol;
	std::string backtexture;
	std::string spritetexture;
	Grid ** GlGrid;
	GLuint backgroundID = -1;
	GLuint SpriteID = -1;
public:
	Map(int row, int col, std::string backgroundfilename);
	~Map();
	void drawbackground();
	void drawgridline();
	void drawgrid();
	int getrow();
	int getcol();
	void setGrid(int row, int col, int type, float * textCoord,string Textid);
	void setSpritefilename(std::string filename);
	void savefile(std::string filename);
	void setSpritesheetrowcol(int row, int col);

};


#pragma once
#ifndef _glpane_
#define _glpane_

#include "GlRenderEditor.h"
#include "MapMeshVbo.h"
#include <memory>
#include "wx/wx.h"
#include "wx/glcanvas.h"
#include "Editor.h"
#include "LineMesh.h"
#include "SpriteMesh.h"


class MyGLCanvas : public wxGLCanvas
{
	int width, height, mapSize;
	wxGLContext*	m_context;
	GlRenderEditor *editorRenderer = nullptr;
	Editor * editor = nullptr;
	int mousex, mousey;
	bool isFirstRender;
	int leftCornerX;
	int leftCornerY;


public:

	MyGLCanvas(wxPanel* parent, Editor * editor);
	void MousePos(wxMouseEvent& event);

	void render(wxPaintEvent& evt);
	void resizeCanvas(wxSizeEvent &event);
	void OnKeyDownEvent(wxChar event);
	void MouseWheel(wxMouseEvent& event);
	DECLARE_EVENT_TABLE()

};
#endif


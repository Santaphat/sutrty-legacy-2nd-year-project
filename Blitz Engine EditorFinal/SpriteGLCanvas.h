#pragma once
#ifndef _spriteglpane_
#define _spriteglpane_

#include "SpriteRenderEditor.h"
#include "MapMeshVbo.h"
#include <memory>
#include "wx/wx.h"
#include "wx/glcanvas.h"
#include "Editor.h"
#include "SpriteMesh.h"
#include "LineMesh.h"
#include "SquareMeshVbo.h"

class SpriteGLCanvas : public wxGLCanvas
{
	int width, height, mapSize;
	wxGLContext*	m_context;
	SpriteRenderEditor *SpriteRenderer = nullptr;
	Editor * editor = nullptr;
	int mousex, mousey;


public:

	SpriteGLCanvas(wxPanel* parent,Editor * editor);
	void setMousePos(int x, int y);
	int getMousePosx();
	int getMOusePosy();

	void render(wxPaintEvent& evt);
	void resizeCanvas(wxSizeEvent &event);
	void OnKeyDownEvent(wxKeyEvent& event);
	void MousePos(wxMouseEvent& event);
	DECLARE_EVENT_TABLE()

};
#endif

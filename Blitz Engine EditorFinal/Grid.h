#pragma once
#include "GlRenderEditor.h"
#include "SpriteMesh.h"

enum {
	NONE,
	PLATFORM,
	COLLISION,
	MONSTER,
	PLAYER,
	ITEM
};
class Grid
{
private:
	int type;
	float * texturecoord;
	int row, col;
	GLuint textureId;
	glm::vec4 color;
public:
	Grid();
	Grid(int type, float * textcoord, int row, int col, GLuint id);
	void draw();
	int getType();
	float  gettextcoord(int i);
};


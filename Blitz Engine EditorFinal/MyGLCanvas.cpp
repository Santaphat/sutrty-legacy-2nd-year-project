#pragma once
#include "MyGlCanvas.h"

MyGLCanvas::MyGLCanvas(wxPanel* parent, Editor * editor) :
	 wxGLCanvas(parent, wxID_ANY, wxDefaultPosition,wxDefaultSize, 0, wxT("GLCanvas"))
{
	wxSize size = this->GetSize();
	width = size.x, height = size.y, mapSize = 11;

	this->editor = editor;

	int argc = 1;
	char* argv[1] = { wxString((wxTheApp->argv)[0]).char_str() };

	m_context = new wxGLContext(this);
	SetCurrent(*m_context);

	editorRenderer = GlRenderEditor::getinstance();
	editorRenderer->initGL("shaders/basic/vertext.shd", "shaders/basic/fragment.shd");
	
	LineMesh * linevbo = new LineMesh;
	linevbo->loadData();
	editorRenderer->addShape(linevbo->getMeshName(), linevbo);

	SpriteMesh * spriteVbo = new SpriteMesh();
	spriteVbo->loadData();
	editorRenderer->addShape(spriteVbo->getMeshName(), spriteVbo);
	isFirstRender = true;

}



void MyGLCanvas::render(wxPaintEvent& evt)
{
	SetCurrent(*m_context);
	if (isFirstRender)
	{
		resizeCanvas(wxSizeEvent());
		isFirstRender = false;
	}
	
	//editorRenderer->render();
	wxPaintDC(this);

	editorRenderer->BeginRender();

	if (editor != nullptr && editor->getMap() != nullptr) {
		editor->getMap()->drawbackground();
		editor->getMap()->drawgrid();
		editor->getMap()->drawgridline();
	}

	editorRenderer->EndRender();




	SwapBuffers();
	
}

void MyGLCanvas::resizeCanvas(wxSizeEvent & event)
{
	wxSize size = this->GetSize();
	editorRenderer->setViewport(size.x, size.y);
	this->Refresh();
}

void MyGLCanvas::OnKeyDownEvent(wxChar event) {
	
	wxChar uc = event;
	if (editor->getMapcurrentstatus() != nullptr) {
		if (uc == 'W') {
			if ((editorRenderer->getOrthoPosy() + editorRenderer->getDefOrthoZoomY()) < editor->getMap()->getrow()) {
				editorRenderer->setOrthoPos(editorRenderer->getOrthoPosx(), editorRenderer->getOrthoPosy() + 1.f);
				this->Refresh();

			}
		}
		else if (uc == 'S') {
			if ((editorRenderer->getOrthoPosy() > 0)) {
				editorRenderer->setOrthoPos(editorRenderer->getOrthoPosx(), editorRenderer->getOrthoPosy() - 1.f);
				this->Refresh();
			}
		}
		else if (uc == 'A') {
			if (editorRenderer->getOrthoPosx() > 0) {
				editorRenderer->setOrthoPos(editorRenderer->getOrthoPosx() - 1.f, editorRenderer->getOrthoPosy());
				this->Refresh();
			}
		}
		else if (uc == 'D') {
			if (editorRenderer->getOrthoPosx() + editorRenderer->getDefOrthoZoomX() < editor->getMap()->getcol()) {
				editorRenderer->setOrthoPos(editorRenderer->getOrthoPosx() + 1.f, editorRenderer->getOrthoPosy());
				this->Refresh();
			}
		}
	}
}
void MyGLCanvas::MouseWheel(wxMouseEvent& event)
{

	int MouseRotation = event.GetWheelRotation();
	if (MouseRotation < 0) {
		if (editor->getMap()->getcol() > editorRenderer->getDefOrthoZoomX() + 100 && editor->getMap()->getrow() > editorRenderer->getDefOrthoZoomY() + 100) {
			editorRenderer->setDefOrthoZoom(editorRenderer->getDefOrthoZoomX() + 5);
		}
		else if (editor->getMap()->getcol() > editorRenderer->getDefOrthoZoomX() + 20 && editor->getMap()->getrow() > editorRenderer->getDefOrthoZoomY() + 20) {
			editorRenderer->setDefOrthoZoom(editorRenderer->getDefOrthoZoomX() + 2);
		}
		else if (editor->getMap()->getcol() > editorRenderer->getDefOrthoZoomX() + 1 && editor->getMap()->getrow() > editorRenderer->getDefOrthoZoomY() + 1)
			editorRenderer->setDefOrthoZoom(editorRenderer->getDefOrthoZoomX() + 1);
	}
	if (MouseRotation > 0) {
		if (0 < editorRenderer->getDefOrthoZoomX() - 100 && editor->getMap()->getrow() > editorRenderer->getDefOrthoZoomY() - 100) {
			editorRenderer->setDefOrthoZoom(editorRenderer->getDefOrthoZoomX() - 5);
		}
		else if (0 < editorRenderer->getDefOrthoZoomX() - 20 && editor->getMap()->getrow() > editorRenderer->getDefOrthoZoomY() - 20) {
			editorRenderer->setDefOrthoZoom(editorRenderer->getDefOrthoZoomX() - 2);
		}
		else if (0 < editorRenderer->getDefOrthoZoomX() - 1 && editor->getMap()->getrow() > editorRenderer->getDefOrthoZoomY() - 1)
			editorRenderer->setDefOrthoZoom(editorRenderer->getDefOrthoZoomX() - 1);
	}
	this->Refresh();

}
void MyGLCanvas::MousePos(wxMouseEvent& event) {

	if (editor->getMap() != nullptr) {
		const wxPoint pt = wxGetMousePosition();
		int x = pt.x - this->GetScreenPosition().x;
		int y = pt.y - this->GetScreenPosition().y;
		//Calculate Row & Col

		int totalcolotho = editorRenderer->getDefOrthoZoomX();
		int totalrowotho = editorRenderer->getDefOrthoZoomY();
		int CanvasWidth = this->GetSize().x;
		int CanvasHeight = this->GetSize().y;

		

		float gridWidth = (float)CanvasWidth / (float)totalcolotho;
		float gridHeight = (float)CanvasHeight / (float)totalrowotho;

		//int row = (int)((y / gridHeight) + (editor->getMap()->getrow() - editorRenderer->getOrthoPosy() - totalrowotho));
		int row = totalrowotho - (int)((y / gridHeight)) - 1 + editorRenderer->getOrthoPosy();
		int col = (int)((x / gridWidth)+ editorRenderer->getOrthoPosx());
		/*row = editor->getMap()->getrow() - row - 1;*/
		/*wxLogMessage("Col:%d Row:%d", col, row);*/
		/*if(editor->getSpriteSheet() != nullptr && editor->getTextCoord() != nullptr && editor->getspritecurrentrow() != NULL)*/
		editor->getMap()->setGrid(row, col, editor->getType(), editor->getTextCoord(),editor->getSpriteSheet()->getFilename());

		this->Refresh();
	}
	/*wxLogMessage("Hi %d %d",x,y);*/
	/*editorRenderer->setMousePos(x, y);*/
}

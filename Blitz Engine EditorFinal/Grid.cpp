#include "Grid.h"
#include "glm/gtc/matrix_transform.hpp"
#include <wx/wfstream.h>
#include "CustomDialog.h"
#include <wx/string.h>



Grid::Grid():Grid(NONE, nullptr, -1, -1,  -1)
{
}

Grid::Grid(int type, float * textcoord,int row, int col, GLuint id){
	this->type = type;
	this->texturecoord = textcoord;
	this->row = row;
	this->col = col;
	this->textureId = id;

	if (type == PLATFORM)
	{
		color = glm::vec4(0.0f, 0.0f, 1.0f, 0.5f);
	}
	else if (type == COLLISION)
		color = glm::vec4(1.0f, 0.0f, 0.0f, 0.5f);
	else if (type == PLAYER)
		color = glm::vec4(0.5f, 0.5f, 0.0f, 0.5f);
	else if (type == MONSTER)
		color = glm::vec4(1.0f, 0.0f, 1.0f, 0.5f);
	else if (type == ITEM)
		color = glm::vec4(1.0f, 1.0f, 0.0f, 0.5f);

}

void Grid::draw(){
	if (type != NONE)
	{
		SpriteMesh * GrRr = dynamic_cast<SpriteMesh*>(GlRenderEditor::getinstance()->getMesh("Sprite"));
		GlRenderEditor::getinstance()->setmode(1);
		//GlRenderEditor::getinstance()->initTexture(textureId);
		//GlRenderEditor::getinstance()->setTexture(textureId);
		GlRenderEditor::getinstance()->setDefaultProjectionMatrix();
		glm::mat4 modelMatrix = glm::mat4(1.f);

		//modelMatrix = glm::scale(modelMatrix, glm::vec3(col, row, 1));
		modelMatrix = glm::translate(modelMatrix, glm::vec3(0.5f + this->col, 0.5f + this->row, 0.0f));
		modelMatrix = GlRenderEditor::getinstance()->getProjectonMatrix()*modelMatrix;
		GlRenderEditor::getinstance()->setModelMatrix(modelMatrix);
		GrRr->setTextureCoord(texturecoord);
		GrRr->render();

		GlRenderEditor::getinstance()->setmode(0);
		GlRenderEditor::getinstance()->setColor(color);
		GrRr->render();
	}
	
}

int Grid::getType()
{
	int i = this->type;
	wxLogMessage("%d",i);
	return this->type;
}

float Grid::gettextcoord(int i)
{
	if (texturecoord == nullptr)
		return 0;
	return texturecoord[i];
}


